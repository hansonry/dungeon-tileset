<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="2020.06.25" name="dungeon-tileset" tilewidth="64" tileheight="64" tilecount="64" columns="8">
 <image source="dungeon-tileset.png" width="512" height="512"/>
 <terraintypes>
  <terrain name="Dungeon Floor" tile="9"/>
  <terrain name="Dungeon Wall" tile="3"/>
 </terraintypes>
 <tile id="0" terrain="1,1,1,0"/>
 <tile id="1" terrain="1,1,0,0"/>
 <tile id="2" terrain="1,1,0,1"/>
 <tile id="3" terrain="1,1,1,1"/>
 <tile id="8" terrain="1,0,1,0"/>
 <tile id="9" terrain="0,0,0,0"/>
 <tile id="10" terrain="0,1,0,1"/>
 <tile id="16" terrain="1,0,1,1"/>
 <tile id="17" terrain="0,0,1,1"/>
 <tile id="18" terrain="0,1,1,1"/>
 <tile id="24" terrain="0,0,0,1"/>
 <tile id="25" terrain="0,0,1,0"/>
 <tile id="32" terrain="0,1,0,0"/>
 <tile id="33" terrain="1,0,0,0"/>
 <wangsets>
  <wangset name="Dungeon Floor" tile="-1"/>
 </wangsets>
</tileset>
